FROM continuumio/miniconda3


WORKDIR /achi
COPY src src
COPY cmake-conda.sh .
RUN chmod +x cmake-conda.sh
COPY conda-recipe/enviroment.yml .
RUN conda env create -f enviroment.yml
RUN echo "conda activate cpp_test" >> ~/.bashrc
SHELL ["/bin/bash", "--login", "-c"]


COPY CMakeLists.txt entrypoint.sh ./
ENTRYPOINT ["./entrypoint.sh"] 
