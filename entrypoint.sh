#!/bin/bash --login

set -euo pipefail
set +euo pipefail
conda activate cpp_test
set -euo pipefail
exec ./cmake-conda.sh