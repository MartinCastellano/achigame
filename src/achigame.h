#ifndef ACHIGAME_HPP
#define ACHIGAME_HPP

#include <climits>
#include <vector>

struct Move
{
  int row;
  int col;
};

class achigame
{
public:
  achigame();
  virtual ~achigame();
  void playGame();
  void setBoardValue(int row, int col, char value);
  char getBoardValue(int row, int col) const;
  const std::vector<std::vector<char>> &getBoard() const;
  void setBoard(const std::vector<std::vector<char>> &newBoard);
  int getMinimax(int depth, bool max_player);
  Move getBestMove();
  void setCurrentPlayer(char player);
  char getWinner();


private:
  char currentPlayer;
  std::vector<std::vector<char>> board{ { ' ', ' ', ' ' },
    { ' ', ' ', ' ' },
    { ' ', ' ', ' ' } };
  int piecesPlaced[2];
  void nexContinue();
  void init();
  bool hasPlayerWon(char player);
  int evaluate();
  int miniMax(int depth, bool max_player);
  Move findBestMove();
  Move findBestRemove();
  void printBoard() const;
  bool isValidMove(int row_a, int col_a, int row_b, int col_b);
  void chooseStartingPlayer();
  bool filledBorad();
  std::vector<Move> getPositionsOfX();
  Move getEmptyPositions();
};

#endif// ACHIGAME_HPP
