
#include "achigame.h"
#include <iostream>

using namespace std;
achigame::achigame() : currentPlayer('X'), piecesPlaced{ 0, 0 } {}
achigame::~achigame() {}
/**
 * @brief Inicia el juego Achi.
 *
 * Esta función se encarga de iniciar y controlar todo el flujo del juego Achi,
 * incluyendo la colocación de fichas, movimientos de los jugadores y determinar
 * el ganador.
 */
void achigame::playGame()
{
  cout << "¡Bienvenido al juego Achi!" << endl;
  cout << "Cada jugador debe colocar sus 4 piezas en el tablero." << endl;
  chooseStartingPlayer();
  init();
  cout << "Ambos jugadores han colocado todas sus piezas. Tablero final:"
       << endl;
  if (filledBorad()) { nexContinue(); }
}
/**
 * @brief Verifica si un movimiento es válido en el tablero.
 * @param row_a La fila de la pieza que se desea mover.
 * @param col_a La columna de la pieza que se desea mover.
 * @param row_b La fila a la que se desea mover la pieza.
 * @param col_b La columna a la que se desea mover la pieza.
 * @return true si el movimiento es válido, false en caso contrario.
 */
bool achigame::isValidMove(int row_a, int col_a, int row_b, int col_b)
{
  if (row_b < 0 || row_b >= 3 || col_b < 0 || col_b >= 3) { return false; }

  if (board[row_b][col_b] != ' ') { return false; }
  if (row_a == row_b && col_a == col_b) { return false; }

  int rowDiff = abs(row_b - row_a);
  int colDiff = abs(col_b - col_a);
  return (rowDiff <= 1 && colDiff <= 1 && rowDiff + colDiff > 0);
}
/**
 * @brief Controla el flujo del juego Achi después de que ambos jugadores han
 * colocado todas sus piezas.
 *
 * Esta función permite que los jugadores realicen movimientos alternativos
 * hasta que uno de ellos gane o haya un empate.
 */
void achigame::nexContinue()
{
  while (true) {

    printBoard();
    if (hasPlayerWon('X')) {
      cout << "¡Jugador X (computadora) ha ganado!" << endl;
      break;
    } else if (hasPlayerWon('O')) {
      cout << "¡Jugador O ha ganado!" << endl;
      break;
    }

    if (currentPlayer == 'X') {

      if (piecesPlaced[1] == 4) {

        std::cout << std::endl;
        int x, y;
        Move bestMoveToRemove = findBestRemove();
        x = bestMoveToRemove.row;
        y = bestMoveToRemove.col;
        int b_x;
        int b_y;

        Move bestMove = findBestMove();
        b_x = bestMove.row;
        b_y = bestMove.col;
        if (isValidMove(x, y, b_x, b_y)) {

          board[x][y] = ' ';

          if (isValidMove(x, y, b_x, b_y)) {
            board[b_x][b_y] = 'X';
          } else {

            cout << "Movimiento inválido. Inténtalo de nuevo." << endl;
            board[x][y] = 'X';
          }

        } else {

          while (true) {

            std::vector<Move> positions = getPositionsOfX();
            int indexToRemove = -1;
            for (std::vector<Move>::size_type i = 0; i < positions.size();
                 i++) {
              if (positions[i].row == x && positions[i].col == y) {
                indexToRemove = i;
                break;
              }
            }
            if (indexToRemove != -1) {

              positions.erase(positions.begin() + indexToRemove);
              x = positions[0].row;
              y = positions[0].col;
            }

            if (isValidMove(x, y, b_x, b_y)) {
              board[x][y] = ' ';
              board[b_x][b_y] = 'X';
            }
            break;
          }
        }

      } else {
        int b_x, b_y;
        Move bestMove = findBestMove();

        b_x = bestMove.row;
        b_y = bestMove.col;

        if (b_x >= 0 && b_x < 3 && b_y >= 0 && b_y < 3
            && board[b_x][b_y] == 'X') {
          board[b_x][b_y] = 'X';
        } else {
          cout << "Movimiento inválido. Inténtalo de nuevo." << endl;
        }
      }

    } else if (currentPlayer == 'O') {

      cout << "Turno del jugador O." << endl;

      int x, y;
      cout << "Jugador " << currentPlayer
           << ", Ingresa la fila de la pieza a mover: ";
      cin >> x;
      cout << "Ingresa la columna de la pieza a mover: ";
      cin >> y;

      if (x >= 0 && x < 3 && y >= 0 && y < 3 && board[x][y] == 'O') {
        int b_x, b_y;
        board[x][y] = ' ';
        cout << "Jugador " << currentPlayer
             << ", Ingresa la fila para colocar la pieza : ";
        cin >> b_x;
        cout << "Ingresa la columna para colocar la pieza: ";
        cin >> b_y;

        if (isValidMove(x, y, b_x, b_y)) {
          board[b_x][b_y] = 'O';
        } else {
          cout << "Movimiento inválido. Inténtalo de nuevo." << endl;
          board[x][y] = 'O';
        }
      } else {
        cout << "Movimiento inválido. Inténtalo de nuevo." << endl;
      }
    }

    // verifico que el tablero siga completo antes de cambiar de jugador
    // if(filledBorad()){
    //     currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
    // }
    currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
  }
}
/**
 * @brief Permite al usuario elegir qué jugador comienza el juego.
 *
 * El jugador puede elegir entre 'X' o 'O' como el primer jugador. Si la
 * selección no es válida, el jugador X (computadora) comenzará por defecto.
 */
void achigame::chooseStartingPlayer()
{
  char startingPlayer;
  cout << "¿Que jugador va a comenzar, X o O? si la seleccion no es valida. "
          "Por defecto, el Jugador X (computadora)";
  cin >> startingPlayer;

  if (startingPlayer == 'X' || startingPlayer == 'x') {
    currentPlayer = 'X';
  } else if (startingPlayer == 'O' || startingPlayer == 'o') {
    currentPlayer = 'O';
  } else if (startingPlayer != ('X' || 'x' || 'O' || 'o')) {
    cout << "Seleccion no valida. Por defecto, el Jugador X (computadora) "
            "comenzara."
         << endl;
    currentPlayer = 'X';
  }
}
/**
 * @brief Inicializa el juego Achi permitiendo que ambos jugadores coloquen sus
 * piezas en el tablero.
 *
 * El jugador X (computadora) coloca sus piezas automáticamente, mientras que el
 * jugador O es solicitado para ingresar sus movimientos.
 */
void achigame::init()
{
  char currentPlayer = this->currentPlayer;
  while (piecesPlaced[0] < 4 || piecesPlaced[1] < 4) {

    int x, y;
    printBoard();
    Move bestMove = findBestMove();
    if (piecesPlaced[0] == 4 && piecesPlaced[1] == 4) { break; }

    if (currentPlayer == 'X') {
      cout << "La computadora coloca la fila " << bestMove.row
           << " y la columna " << bestMove.col << "." << endl;
      x = bestMove.row;
      y = bestMove.col;

    } else {

      cout << "Jugador " << currentPlayer
           << ", Ingresa la fila para colocar la pieza : ";
      cin >> x;
      cout << "Ingresa la columna para colocar la pieza: ";
      cin >> y;
    }

    if (x >= 0 && x < 3 && y >= 0 && y < 3 && board[x][y] == ' ') {
      board[x][y] = currentPlayer;
      piecesPlaced[currentPlayer == 'X' ? 0 : 1]++;
      currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
    } else {
      cout << "Movimiento inválido. Inténtalo de nuevo." << endl;
    }
    evaluate();
  }
}
/**
 * @brief Comprueba si un jugador ha ganado el juego.
 * @param board El tablero de juego.
 * @param player El jugador a comprobar ('X' o 'O').
 * @return true si el jugador ha ganado, false en caso contrario.
 */
bool achigame::hasPlayerWon(char player)
{

  for (int i = 0; i < 3; ++i) {
    if ((board[i][0] == player && board[i][1] == player
          && board[i][2] == player)
        || (board[0][i] == player && board[1][i] == player
            && board[2][i] == player)) {
      return true;
    }
  }

  if ((board[0][0] == player && board[1][1] == player && board[2][2] == player)
      || (board[0][2] == player && board[1][1] == player
          && board[2][0] == player)) {
    return true;
  }
  return false;
}

/**
 * @brief Evalúa la situación actual del juego.
 * @param board El tablero de juego.
 * @return 1 si 'X' ha ganado, -1 si 'O' ha ganado.
 */
int achigame::evaluate()
{
  if (hasPlayerWon('X')) {
    return 1;
  } else if (hasPlayerWon('O')) {
    return -1;
  }
  return 0;
}
/**
 * @brief Verifica si ambos jugadores han colocado todas sus piezas en el
 * tablero.
 * @return true si ambos jugadores han colocado sus 4 piezas, false en caso
 * contrario.
 */
bool achigame::filledBorad()
{
  int playerXPiecesCount = 0;
  int playerOPiecesCount = 0;

  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      if (board[i][j] == 'X') {
        playerXPiecesCount++;
      } else if (board[i][j] == 'O') {
        playerOPiecesCount++;
      }
    }
  }

  if (playerXPiecesCount == 4 && playerOPiecesCount == 4) {
    return true;
  } else {
    return false;
  }
}
/**
 * @brief Implementa el algoritmo Minimax para encontrar la mejor jugada.
 * @link https://es.wikipedia.org/wiki/Minimax
 * @param board El tablero de juego.
 * @param depth La profundidad actual en el árbol de búsqueda.
 * @param max_player true si es el turno del jugador maximizador ('X'), false si
 * es el turno del minimizador ('O').
 * @return El puntaje de la mejor jugada posible.
 */
int achigame::miniMax(int depth, bool max_player)
{
  char player = (max_player) ? 'X' : 'O';
  int bestScore = (max_player) ? -INT_MAX : INT_MAX;

  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      if (board[i][j] == ' ') {
        board[i][j] = player;
        int score = miniMax(depth - 1, !max_player);
        board[i][j] = ' ';

        if (max_player) {
          bestScore = max(bestScore, score);
        } else {
          bestScore = min(bestScore, score);
        }
      }
    }
  }

  return bestScore;
}

/**
 * @brief Encuentra la mejor jugada para el jugador actual.
 * @param board El tablero de juego.
 * @return La mejor jugada a realizar.
 */
Move achigame::findBestMove()
{
  char player = currentPlayer;

  int bestScore = -INT_MAX;
  Move bestMove;
  bool may_palyer = false;
  if (player == 'O') { may_palyer = true; }

  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      if (board[i][j] == ' ') {
        board[i][j] = player;
        int score = miniMax(3, may_palyer);
        board[i][j] = ' ';

        if (score > bestScore) {
          bestScore = score;
          bestMove.row = i;
          bestMove.col = j;
        }
      }
    }
  }

  return bestMove;
}

/**
 * @brief Encuentra la mejor jugada a remover para el jugador actual.
 * @param board El tablero de juego.
 * @return La mejor jugada a remover.
 */
Move achigame::findBestRemove()
{

  int bestScore = -INT_MAX;
  Move bestMoveToRemove;

  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      if (board[i][j] == 'X') {
        board[i][j] = ' ';

        int score = miniMax(3, false);

        board[i][j] = 'X';

        if (score > bestScore) {
          bestScore = score;
          bestMoveToRemove.row = i;
          bestMoveToRemove.col = j;
        }
      }
    }
  }

  return bestMoveToRemove;
}

/**
 * @brief Imprime el estado actual del tablero de juego.
 * @param board El tablero de juego.
 */
void achigame::printBoard() const
{
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      cout << " ";
      if (board[i][j] == ' ')
        cout << " ";
      else
        cout << board[i][j];
      cout << " ";
      if (j < 2) cout << "|";
    }
    cout << endl;
    if (i < 2) cout << "-----------" << endl;
  }
  cout << endl;
}
/**
 * @brief obtiene la posicion que no tiene ninguna ficha.
 * @return fila y columna donde no hay fichas.
 */
Move achigame::getEmptyPositions()
{
  Move empty;

  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      if (board[i][j] == ' ') {

        empty.row = i;
        empty.col = j;
      }
    }
  }

  return empty;
}

/**
 * @brief obtiene todas las posiciones donde hay fichas X
 * @return Un vector de Move que almacenan filas y columnas de las posiciones
 * 'X'
 */
std::vector<Move> achigame::getPositionsOfX()
{
  std::vector<Move> positions;

  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      if (board[i][j] == 'X') {
        Move move;
        move.row = i;
        move.col = j;
        positions.push_back(move);
      }
    }
  }

  return positions;
}
/**
 * @brief Setea un valor determinado en el tablero.
 */
void achigame::setBoardValue(int row, int col, char value)
{
  board[row][col] = value;
}
/**
 * @brief Obtiene un valor determinado del tablero.
 * @return char
 */
char achigame::getBoardValue(int row, int col) const { return board[row][col]; }
const std::vector<std::vector<char>> &achigame::getBoard() const
{
  return board;
}
/**
 * @brief Setea un nuevo tablero por defecto
 */
void achigame::setBoard(const std::vector<std::vector<char>> &newBoard)
{

  if (newBoard.size() == 3 && newBoard[0].size() == 3) {
    board = newBoard;
  } else {
    cerr << "Error: El tablero no es de las diemnciones que corresponde."
         << endl;
  }
}
/**
 * @brief retorna el valor del puntaje
 * @return entero -/+
 */
int achigame::getMinimax(int depth, bool max_player)
{
  return miniMax(depth, max_player);
}
/**
 * @brief me retorna el mejor movimiento para ese jugador, usa minimax
 * @return movimiento fila columna
 */
Move achigame::getBestMove() { 
  return findBestMove(); 
}

void achigame::setCurrentPlayer(char player)
{
  if (player == 'X' || player == 'O') {
    currentPlayer = player;
  } else {
    cout << "Selección no válida. El jugador actual es: " << currentPlayer
         << "." << endl;
  }
}
/**
 * @brief retorna al ganador para determiando tablero si es que lo hay
 * @return char
 */
char achigame::getWinner()
{
  if (hasPlayerWon('X')) {
    return 'X';
  } else if (hasPlayerWon('O')) {
    return 'O';
  }
  return ' ';
}
