#include "../src/achigame.cpp"
#include <gtest/gtest.h>
using namespace std;


TEST(Achigame, miniMax)
{
  achigame game;
  std::vector<std::vector<char>> newBoard = {
    { 'X', 'O', 'X' },
    { ' ', 'X', 'O' },
    { 'O', 'X', ' ' },
  };
  game.setBoard(newBoard);
  int result = game.getMinimax(3, false);
  int score_expected = 2147483647;

  EXPECT_EQ(result, score_expected);
}
TEST(Achigame, bestMove)
{
  achigame game;
  std::vector<std::vector<char>> newBoard = {
    { 'X', 'O', 'X' },
    { ' ', 'X', 'O' },
    { 'O', 'X', ' ' },
  };
  game.setBoard(newBoard);
  int x = 1;
  int y = 0;
  game.setCurrentPlayer('O');
  Move bestMove = game.getBestMove();
  EXPECT_EQ(bestMove.row, x);
  EXPECT_EQ(bestMove.col, y);
}
TEST(Achigame, oWins)
{

  achigame game;
  std::vector<std::vector<char>> newBoard = {
    { 'X', 'O', 'X' },
    { 'X', 'X', ' ' },
    { 'O', 'O', 'O' },
  };
  game.setBoard(newBoard);
  char winner = game.getWinner();
  
  EXPECT_EQ(winner, 'O');
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
