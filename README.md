# Achi - Cómo Jugar

Achi es un juego de mesa tradicional para dos jugadores originario de Ghana, que comparte similitudes con juegos como el tres en línea (Tic-Tac-Toe) y el Morris de los Nueve Hombres (Nine Men's Morris). Se juega en una cuadrícula de 3x3, y el objetivo es formar una línea recta de tres piezas, ya sea horizontalmente, verticalmente o diagonalmente.

## Tabla de Contenidos
- [Introducción](#introducción)
- [Configuración](#configuración)
- [Mecánica del Juego](#mecánica-del-juego)
- [Ganar](#ganar)
- [Ejemplos](#ejemplos)

## Introducción

Achi es un juego sencillo pero cautivador para dos jugadores. Cada jugador tiene cuatro piezas de colores distintos y se turnan para mover sus piezas en el tablero. El tablero de juego consta de una cuadrícula de 3x3 con nueve intersecciones.

![Imagen de Achi](https://static.wixstatic.com/media/ef62e5_98815a13d867436792c07970c8589795~mv2.jpg/v1/fill/w_360,h_358,al_c,q_80,usm_0.66_1.00_0.01,enc_auto/ef62e5_98815a13d867436792c07970c8589795~mv2.jpg)

## Configuración

1. **Jugadores**: Achi se juega con dos jugadores, cada uno con cuatro piezas de diferentes colores (por ejemplo, negro y blanco).

2. **Configuración del Tablero**: El tablero de juego es una cuadrícula de 3x3 con nueve intersecciones. Los jugadores comienzan colocando sus cuatro piezas en las intersecciones del tablero, dejando una intersección vacía.

## Mecánica del Juego

1. **Objetivo**: El objetivo principal de Achi es formar una línea recta de tres piezas de tu color, ya sea horizontalmente, verticalmente o diagonalmente.

2. **Turnos**: Los jugadores se turnan, comenzando con un jugador que realiza un movimiento y luego alternando.

3. **Mover Piezas**: En tu turno, puedes mover una de tus piezas a una intersección adyacente vacía a lo largo de las líneas del tablero. Puedes mover tu pieza horizontalmente, verticalmente o diagonalmente, siempre y cuando la intersección de destino esté vacía.

4. **Ejemplo de Movimiento**: Supongamos que tienes una pieza blanca en el centro del tablero, y la esquina superior izquierda está vacía. Puedes mover tu pieza a la esquina superior izquierda en un solo movimiento.

## Ganar

El juego de Achi termina cuando se cumple una de las siguientes condiciones:

1. **Tres en Línea**: Si un jugador logra formar una línea recta de tres de sus piezas (horizontal, vertical o diagonalmente), gana el juego.

2. **Sin Movimientos Legales Restantes**: Si ningún jugador puede formar una línea de tres piezas y no quedan intersecciones vacías para movimientos legales, el juego termina en empate.

## Ejemplos

### Ejemplo 1: Configuración del Tablero

```
Tablero de Juego Inicial:
  1 | 2 | 3
  4 |   | 6
  7 | 8 | 9
```

En este ejemplo, el tablero está listo para comenzar el juego. El Jugador 1 tiene piezas negras y el Jugador 2 tiene piezas blancas. La intersección central está vacía.

### Ejemplo 2: Realizar un Movimiento

```
Tablero de Juego Actual:
  X | 2 | 3
  4 |   | 6
  7 | 8 | 9
```

El Jugador 1 (negro) decide realizar un movimiento y coloca su pieza en la posición 1. Ahora, el tablero luce así, con la pieza del Jugador 1 representada como 'X'.

### Ejemplo 3: Ganar el Juego

```
Tablero de Juego Actual:
  X | X | X
  4 |   | 6
  7 | 8 | 9
```

El Jugador 1 (negro) ha alineado con éxito tres de sus piezas horizontalmente. Como resultado, gana el juego.

## NOTAS

* El momiviento del oponenete hace uso del algoritmo  minimax para la busqueda del mejor movimiento.

* Antes de comenzar asegurate de crear el ambiente conda

```
conda env create  -f conda-recipe/enviroment.yml

```
y activarlo

```
conda activate cpp_test 
```

# Ahora a JUGAR!!
para compilar y ejecutar la partida hacer
```
  ./cmake-conda.sh   
```
para correr los test antes
```
 ./cmake-conda.sh TEST_ON 
```
# Dockerfile
En caso de no contar con conda se debe contar con docker
y se debe hacer
(esto puede tardar un momento)
```
docker build -t achi .
```
y luego
```
docker run -it achi
```

## Tecnologias utilizadas

* Codigo en C++17.

* Separacion por cabezera.

* Uso de cmake.

* Docker

* Conda

* Doxygen

* pre-commit

