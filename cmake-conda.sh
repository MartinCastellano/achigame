#!/usr/bin/env bash
#                _     _         _____                      
#      /\       | |   (_)       / ____|                     
#     /  \   ___| |__  _ ______| |  __  __ _ _ __ ___   ___ 
#    / /\ \ / __| '_ \| |______| | |_ |/ _` | '_ ` _ \ / _ \
#   / ____ \ (__| | | | |      | |__| | (_| | | | | | |  __/
#  /_/    \_\___|_| |_|_|       \_____|\__,_|_| |_| |_|\___|

##
# @file        cmake-conda.sh
# @brief       Script para compilar y ejecutar el programa Achi-Game.
# @author      Martin Castellano
# @date        2023-09-18
##                                                      
DOXYGEN_CONFIG=doc_config.dox
if [ -f $DOXYGEN_CONFIG ]; then    
    doxygen $DOXYGEN_CONFIG
else
    echo "Error: El archivo de configuración de Doxygen '$DOXYGEN_CONFIG' no se encontró."
fi
echo "Iniciando el proceso de compilación..." 
if [[ "$1" == "TEST_ON" ]]; then
    TEST_ENABLED=ON
    echo "Habilitando las pruebas."
else
    TEST_ENABLED=OFF
    echo "Deshabilitando las pruebas."
fi                                                      
set -ex
BUILD_DIR="build"
cd $(dirname $0)

mkdir -p $BUILD_DIR
cd $BUILD_DIR
cmake -DCMAKE_PREFIX_PATH=$CONDA_PREFIX/$HOST/sysroot/usr \
      -DCMAKE_INSTALL_PREFIX=$CONDA_PREFIX \
      -DTEST_ENABLED=$TEST_ENABLED \
      ..

make -j8


echo "Programa compilado... run... "

if [[ "$TEST_ENABLED" == 'ON' ]]; then
    echo "Corriendo las pruebas."
    exec ./test/tests

fi 
echo "Comenzando el juego..."  
./achi
